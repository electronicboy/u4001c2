package com.shane.trainSystem;

public class Seat {

    private String seatPosition;
    private int seatClass;
    private Boolean seatAisle;
    private Boolean seatTable;
    private Boolean seatForward;
    private Boolean seatEase;
    private String seatBooking;


    public Seat(String seatPosition, int seatClass, Boolean seatAisle, Boolean seatTable, Boolean seatForward, Boolean seatEase, String seatBooking) {
        this.seatPosition = seatPosition;
        this.seatClass = seatClass;
        this.seatAisle = seatAisle;
        this.seatTable = seatTable;
        this.seatForward = seatForward;
        this.seatEase = seatEase;
        this.seatBooking = seatBooking;

    }


    public String getSeatLocation() {
        return seatPosition;
    }

    public void setSeatLocation(String seatPosition) {
        this.seatPosition = seatPosition;
    }

    public int getSeatClass() {
        return seatClass;
    }

    public void setSeatClass(int seatClass) {
        this.seatClass = seatClass;
    }

    public Boolean getSeatAisle() {
        return seatAisle;
    }

    public void setSeatAisle(Boolean seatAisle) {
        this.seatAisle = seatAisle;
    }

    public Boolean getSeatTable() {
        return seatTable;
    }

    public void setSeatTable(Boolean seatTable) {
        this.seatTable = seatTable;
    }

    public Boolean getSeatForward() {
        return seatForward;
    }

    public void setSeatForward(Boolean seatForward) {
        this.seatForward = seatForward;
    }

    public Boolean getSeatEase() {
        return seatEase;
    }

    public void setSeatEase(Boolean seatEase) {
        this.seatEase = seatEase;
    }

    public String getSeatBooking() {
        return seatBooking;
    }

    public void setSeatBooking(String seatBooking) {
        this.seatBooking = seatBooking;
    }

    @Override
    public String toString() {
        return seatPosition + "|" + String.valueOf(seatClass) + "|" + String.valueOf(seatAisle) + "|" + String.valueOf(seatTable) + "|" + String.valueOf(seatForward) + "|" + String.valueOf(seatEase) + "|" + seatBooking;
    }

}
