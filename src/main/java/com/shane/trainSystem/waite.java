package com.shane.trainSystem;

public class Waite {

    private int waiteClass;
    private Boolean waiteAisle;
    private Boolean waiteTable;
    private Boolean waiteForward;
    private Boolean waiteEase;
    private String waiteEmail;


    public Waite(int waiteClass, Boolean waiteAisle, Boolean waiteTable, Boolean waiteForward, Boolean waiteEase, String waiteEmail) {
        this.waiteClass = waiteClass;
        this.waiteAisle = waiteAisle;
        this.waiteTable = waiteTable;
        this.waiteForward = waiteForward;
        this.waiteEase = waiteEase;
        this.waiteEmail = waiteEmail;

    }


    public int getWaiteeClass() {
        return waiteClass;
    }

    public void setWaiteeClass(int Class) {
        this.waiteClass = Class;
    }

    public Boolean getWaiteeAisle() {
        return waiteAisle;
    }

    public void setWaiteeAisle(Boolean Aisle) {
        this.waiteAisle = Aisle;
    }

    public Boolean getWaiteeTable() {
        return waiteTable;
    }

    public void setWaiteeTable(Boolean Table) {
        this.waiteTable = Table;
    }

    public Boolean getWaiteeForward() {
        return waiteForward;
    }

    public void setWaiteeForward(Boolean Forward) {
        this.waiteForward = Forward;
    }

    public Boolean getWaiteeEase() {
        return waiteEase;
    }

    public void setWaiteeEase(Boolean Ease) {
        this.waiteEase = Ease;
    }

    public String getWaiteeEmail() {
        return waiteEmail;
    }

    public void setWaiteEmail(String Email) {
        this.waiteEmail = Email;
    }

    @Override
    public String toString() {
        return String.valueOf(waiteClass) + "|" + String.valueOf(waiteAisle) + "|" + String.valueOf(waiteTable) + "|" + String.valueOf(waiteForward) + "|" + String.valueOf(waiteEase) + "|" + waiteEmail;
    }

}
