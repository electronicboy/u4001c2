package com.shane.trainSystem;

import java.util.Scanner;

public class Main {


    public static void main(String[] Args) {

        Scanner keyboardIn = new Scanner(System.in);
        String keyboardChar;
        Seats Seats = new Seats();

        Seats.loadSeats();


        while (true) {

            System.out.println("============================");
            System.out.println("| Train Reservation System |");
            System.out.println("============================");
            System.out.println("| (1) Create Bookings      |");
            System.out.println("| (2) Cancel Bookings      |");
            System.out.println("| (3) View Bookings        |");
            System.out.println("| (4) View Waiting         |");
            System.out.println("| (5) Process Waiting      |");
            System.out.println("============================");


            keyboardChar = keyboardIn.nextLine();
            Bookings Bookings = new Bookings();
            WaitingList waitingList = new WaitingList();

            switch (keyboardChar) {
                case "1":
                    Bookings.searchPrompt(keyboardIn, Seats);
                    break;
                case "2":
                    Bookings.cancelBooking(keyboardIn, Seats);
                    break;
                case "3":
                    Bookings.viewBookings(keyboardIn, Seats);
                    break;
                case "4":
                    waitingList.loadWaites();
                    waitingList.printWaites();
                    break;
                case "5":
                    waitingList.loadWaites();
                    waitingList.process();
                    waitingList.saveWaites();
                default:
                    break;
            }

        }
    }
}
