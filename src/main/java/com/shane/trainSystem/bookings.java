package com.shane.trainSystem;

import java.util.*;

public class Bookings {


    public void searchPrompt(Scanner keyboardIn, Seats seats) {

        //Prompt the user until they provide a valid email address
        String userEmail;
        do {
            println("Enter your Email address");
            userEmail = keyboardIn.nextLine();
        } while (!userEmail.contains("@"));

        //Check if the user has a ticket class preference
        String userTicketClassString;
        int userTicketClass = 2;
        do {
            println("Select a Ticket Class: (F)irst, (S)econd, (E)ither");
            userTicketClassString = keyboardIn.nextLine();
        }
        while (!(userTicketClassString.toUpperCase().startsWith("F") || userTicketClassString.toUpperCase().startsWith("S") || userTicketClassString.toUpperCase().startsWith("E")));

        if (userTicketClassString.toUpperCase().startsWith("F")) {
            userTicketClass = 1;
        } else if (userTicketClassString.toUpperCase().startsWith("E")) {
            userTicketClass = -1;
        }


        //Check if the user has a seat preference
        Boolean userSeatAisle = null;
        String userSeatAisleString;
        do {
            println("Select Seat Position: (A)isle, (W)indow, (E)ither");
            userSeatAisleString = keyboardIn.nextLine();
        }
        while (!(userSeatAisleString.toUpperCase().startsWith("A") || userSeatAisleString.toUpperCase().startsWith("W") || userSeatAisleString.toUpperCase().startsWith("E")));

        if (userSeatAisleString.toUpperCase().startsWith("A")) {
            userSeatAisle = true;
        } else if (userSeatAisleString.toUpperCase().startsWith("W")) {
            userSeatAisle = false;
        }


        //check if the user has a table preference
        Boolean userSeatTable = null;
        String userSeatTableString;
        do {
            println("Select table requirement: (Y)es, (N)o, (E)ither");
            userSeatTableString = keyboardIn.nextLine();
        }
        while (!(userSeatTableString.toUpperCase().startsWith("Y") || userSeatTableString.toUpperCase().startsWith("N") || userSeatTableString.toUpperCase().startsWith("E")));


        if (userSeatTableString.toUpperCase().startsWith("Y")) {
            userSeatTable = true;
        } else if (userSeatTableString.toUpperCase().startsWith("N")) {
            userSeatTable = false;
        }


        //Check if the user has a preference over the direction they face
        Boolean userSeatDirection = null;
        String userSeatDirectionString;
        do {
            println("Do you have a preference on the travel direction: (F)orward, (B)ackwards, (N)o");
            userSeatDirectionString = keyboardIn.nextLine();
        }
        while (!(userSeatDirectionString.toUpperCase().startsWith("F") || userSeatDirectionString.toUpperCase().startsWith("B") || userSeatDirectionString.toUpperCase().startsWith("N")));

        if (userSeatAisleString.toUpperCase().startsWith("F")) {
            userSeatDirection = true;
        } else if (userSeatAisleString.toUpperCase().startsWith("B")) {
            userSeatDirection = false;
        }


        //Check if the user require easy of access?
        Boolean userSeatEase = false;
        String userSeatEaseString;
        do {
            println("Do you require ease of access: (Y)es, (N)o");
            userSeatEaseString = keyboardIn.nextLine();
        }
        while (!(userSeatEaseString.toUpperCase().startsWith("Y") || userSeatEaseString.toUpperCase().startsWith("N")));

        if (userSeatEaseString.toUpperCase().startsWith("Y")) {
            userSeatEase = true;
        }


        Seats search = search(seats, userTicketClass, userSeatAisle, userSeatTable, userSeatDirection, userSeatEase);

        List<String> availableAisles = new ArrayList<>();

        if (search.getSeats().size() != 0) {

            printHeader();
            for (Seat searchResult : search.getSeats()) {
                availableAisles.add(searchResult.getSeatLocation());
                printRow(searchResult);


            }

            printFooter();

            println("Do you wish to make a booking? (0) to cancel, Or select a seat #");

            String userSelection;

            do {
                userSelection = keyboardIn.nextLine();
            } while (!(userSelection.equals("0") || availableAisles.contains(userSelection)));

            if (userSelection.equals("0"))
                return;

            if (availableAisles.contains(userSelection)) {

                for (Seat seat : seats.getSeats()) {
                    if (seat.getSeatLocation().equals(userSelection)) {
                        seat.setSeatBooking(userEmail);
                        println("Your booking has been stored!");
                        break;
                    }
                }
            }

            seats.saveSeats();
            return;

        } else {
            Seats searchWait = searchWait(seats, userTicketClass, userSeatAisle, userSeatTable, userSeatDirection, userSeatEase);

            if (searchWait.getSeats().size() > 0) {
                printHeader();


                for (Seat seatWait : searchWait.getSeats()) {
                    availableAisles.add(seatWait.getSeatLocation());
                    printRow(seatWait);
                }

                printFooter();
                println("We couldn't find an perfect match, but maybe we can interest you? (0) cancel/queue, Or select a seat #");


                String userSelection;

                do {
                    userSelection = keyboardIn.nextLine();
                } while (!(userSelection.equals("0") || availableAisles.contains(userSelection)));

                if (userSelection.equals("0")) {
                    println("==========================");
                    println("| (1) Cancel booking     |");
                    println("| (2) Enter Queue        |");
                    println("==========================");

                    do {
                        userSelection = keyboardIn.nextLine();
                    } while (!(userSelection.contains("1") || userSelection.contains("2")));

                    if (userSelection.equals("1"))
                        return;

                    if (userSelection.equals("2")) {

                        WaitingList waitingList = new WaitingList();
                        waitingList.loadWaites();

                        Waite Waite = new Waite(userTicketClass, userSeatAisle, userSeatTable, userSeatDirection, userSeatEase, userEmail);
                        waitingList.addWaite(Waite);

                        waitingList.saveWaites();
                        println("You have been added to the waiting list!");
                    }

                }

                if (availableAisles.contains(userSelection)) {
                    saveBooking(seats, userSelection, userEmail);
                }

            } else {

                WaitingList waitingList = new WaitingList();
                waitingList.loadWaites();

                Waite Waite = new Waite(userTicketClass, userSeatAisle, userSeatTable, userSeatDirection, userSeatEase, userEmail);
                waitingList.addWaite(Waite);

                waitingList.saveWaites();
                println("You have been added to the waiting list!");

            }

        }


    }

    public Seats search(Seats seats, int seatClass, Boolean seatAisle, Boolean seatTable, Boolean seatForward, Boolean seatEase) {


        Seats searchSeats = new Seats();

        for (Seat seat : seats.getSeats()) {

            if ((seat.getSeatClass() == -1 || seatClass == seat.getSeatClass()) &&
                    (seatAisle == null || seatAisle == seat.getSeatAisle()) &&
                    (seatTable == null || seatTable == seat.getSeatTable()) &&
                    (seatForward == null || seatForward == seat.getSeatForward()) &&
                    seatEase == seat.getSeatEase() &&
                    seat.getSeatBooking().equals("UNBOOKED")) {

                searchSeats.addSeat(seat);
            }
        }

        return searchSeats;
    }


    public Seats searchWait(Seats seats, int seatClass, Boolean seatAisle, Boolean seatTable, Boolean seatForward, Boolean seatEase) {

        Seats searchSeats = new Seats();

        int HighPoints = 0;

        for (Seat seat : seats.getSeats()) {
            if (seat.getSeatBooking().equals("UNBOOKED") && (seat.getSeatEase() == seatEase)) {
                int points = 0;


                if (seat.getSeatClass() == -1 || seatClass == seat.getSeatClass())
                    points++;

                if (seatAisle == null || seatAisle == seat.getSeatAisle())
                    points++;

                if (seatTable == null || seatTable == seat.getSeatTable())
                    points++;

                if (seatForward == null || seatForward == seat.getSeatForward())
                    points++;

                if (seatEase == seat.getSeatEase())
                    points++;

                println(String.valueOf(points));
                if (points == HighPoints) {
                    searchSeats.addSeat(seat);
                } else if (points > HighPoints) {
                    searchSeats.clearSeats();
                    HighPoints = points;
                    searchSeats.addSeat(seat);

                }
            }
        }

        return searchSeats;
    }


    public void cancelBooking(Scanner keyboardIn, Seats seats) {


        String userEmail;
        println("Please Enter your email address:");
        do {
            userEmail = keyboardIn.nextLine();
        } while (!(userEmail.contains("@")));
        Seats searchResults = new Seats();

        for (Seat seat : seats.getSeats()) {
            if (seat.getSeatBooking().toUpperCase().equals(userEmail.toUpperCase())) {
                searchResults.addSeat(seat);
            }
        }

        if (searchResults.getSeats().size() > 0) {

            List<String> rows = new ArrayList<>();
            for (Seat seat : searchResults.getSeats()) {

                String formt;
                println("=================================================================");
                println("| Seat # | Class| Aisle Seat | seat Direction | Ease of Access? |");
                println("=================================================================");
                formt = "|  %2s    | %3s  | %-3s        | %-9s      | %-3s             |\n";

                for (Seat searchResult : searchResults.getSeats()) {
                    rows.add(searchResult.getSeatLocation());

                    String resultClass;
                    if (searchResult.getSeatClass() == 1) {
                        resultClass = "1st";
                    } else {
                        resultClass = "2nd";
                    }

                    String AisleSeat;
                    if (searchResult.getSeatAisle()) {
                        AisleSeat = "Yes";
                    } else {
                        AisleSeat = "No";
                    }

                    String resultDirection;
                    if (searchResult.getSeatForward()) {
                        resultDirection = "Forward";
                    } else {
                        resultDirection = "Backwards";
                    }

                    String resultEase;
                    if (searchResult.getSeatEase()) {
                        resultEase = "Yes";
                    } else {
                        resultEase = "No";
                    }

                    System.out.printf(formt, searchResult.getSeatLocation(), resultClass, AisleSeat, resultDirection, resultEase);

                }
                println("=================================================================");
                println("Do you wish to cancel a booking? (0) to cancel, or # to cancel");

                String userSelection;
                do {
                    userSelection = keyboardIn.nextLine();
                } while (!(userSelection.equals("0") || rows.contains(userSelection)));

                if (userSelection.equals("0"))
                    return;

                if (rows.contains(userSelection)) {
                    for (Seat seatTrim : seats.getSeats())
                        if (seatTrim.getSeatLocation().equals(userSelection)) {
                            seatTrim.setSeatBooking("UNBOOKED");
                            println("Your booking has been cancelled!");
                            seats.saveSeats();
                            break;
                        }
                }


            }

        } else {
            println("No results found for your email address!");
        }


    }


    // System.out.println wrapper
    private void println(String string) {
        System.out.println(string);
    }

    public void viewBookings(Scanner keyboardIn, Seats seats) {


        Seats seatsResults = new Seats();

        for (Seat search : seats.getSeats()) {
            if (!search.getSeatBooking().equals("UNBOOKED")) {
                seatsResults.addSeat(search);
            }
        }


        String formt;
        println("=================================================================================================");
        println("| Seat # | Class| Aisle Seat | seat Direction | Ease of Access? | Email                         |");
        println("=================================================================================================");
        formt = "|  %2s    | %3s  | %-3s        | %-9s      | %-3s             | %-29s |\n";

        for (Seat seat : seatsResults.getSeats()) {
            String resultClass;
            if (seat.getSeatClass() == 1) {
                resultClass = "1st";
            } else {
                resultClass = "2nd";
            }

            String AisleSeat;
            if (seat.getSeatAisle()) {
                AisleSeat = "Yes";
            } else {
                AisleSeat = "No";
            }

            String resultDirection;
            if (seat.getSeatForward()) {
                resultDirection = "Forward";
            } else {
                resultDirection = "Backwards";
            }

            String resultEase;
            if (seat.getSeatEase()) {
                resultEase = "Yes";
            } else {
                resultEase = "No";
            }

            System.out.printf(formt, seat.getSeatLocation(), resultClass, AisleSeat, resultDirection, resultEase, seat.getSeatBooking());

        }
        println("=================================================================================================");
    }


    //Print the header
    private void printHeader() {
        println("=================================================================");
        println("| Seat # | Class| Aisle Seat | seat Direction | Ease of Access? |");
        println("=================================================================");
    }

    //print a row, Convert to readable text
    private void printRow(Seat seat) {
        String formt;
        formt = "|  %2s    | %3s  | %-3s        | %-9s      | %-3s             |\n";


        String resultClass;
        if (seat.getSeatClass() == 1) {
            resultClass = "1st";
        } else {
            resultClass = "2nd";
        }

        String AisleSeat;
        if (seat.getSeatAisle()) {
            AisleSeat = "Yes";
        } else {
            AisleSeat = "No";
        }

        String resultDirection;
        if (seat.getSeatForward()) {
            resultDirection = "Forward";
        } else {
            resultDirection = "Backwards";
        }

        String resultEase;
        if (seat.getSeatEase()) {
            resultEase = "Yes";
        } else {
            resultEase = "No";
        }

        System.out.printf(formt, seat.getSeatLocation(), resultClass, AisleSeat, resultDirection, resultEase);


    }

    //print the footer
    private void printFooter() {
        println("=================================================================");
    }


    private void saveBooking(Seats seats, String userSelection, String userEmail) {
        for (Seat seat : seats.getSeats()) {
            if (seat.getSeatLocation().equals(userSelection)) {
                seat.setSeatBooking(userEmail);
                println("Your booking has been stored!");
                seats.saveSeats();
                break;
            }
        }
    }
}
