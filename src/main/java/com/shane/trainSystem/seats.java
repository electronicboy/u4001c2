package com.shane.trainSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Seats {

    List<Seat> Seats = new ArrayList<Seat>();
    private File seatsFile = new File("data/seats.txt");
    private Scanner seatsFileScanner;
    private Boolean dataLoaded = false;

    public void loadSeats() {
        try {
            seatsFileScanner = new Scanner(seatsFile);

            while (seatsFileScanner.hasNextLine()) {

                String line = seatsFileScanner.nextLine();
                String[] Data;
                Data = line.split("\\|");

                String seatPosition = Data[0];
                String seatClass = Data[1];
                Boolean seatAisle = Boolean.parseBoolean(Data[2]);
                Boolean seatTable = Boolean.parseBoolean(Data[3]);
                Boolean seatForward = Boolean.parseBoolean(Data[4]);
                Boolean seatEase = Boolean.parseBoolean(Data[5]);
                String seatBooking = Data[6];


                Seat seat = new Seat(seatPosition, Integer.valueOf(seatClass), seatAisle, seatTable, seatForward, seatEase, seatBooking);
                Seats.add(seat);

            }

            seatsFileScanner.close();
            dataLoaded = true;

        } catch (FileNotFoundException e) {
            System.out.println("Seats file not found!");
            System.exit(1);
        }

    }

    public void saveSeats() {

        try {
            PrintWriter saveWriter = new PrintWriter(seatsFile);

            for (Seat seat : Seats) {
                saveWriter.println(seat.toString());
            }

            saveWriter.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addSeat(Seat seat) {
        Seats.add(seat);
    }

    public List<Seat> getSeats() {
        return Seats;
    }

    public void clearSeats() {
        Seats.clear();
    }


    public List<Seat> searchSeats() {

        return null;

    }
}
