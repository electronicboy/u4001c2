package com.shane.trainSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class WaitingList {


    private List<Waite> Waites = new ArrayList<Waite>();
    private File waiteFile = new File("data/waitingList.txt");
    private Scanner waiteFileScanner;
    private Boolean dataLoaded = false;

    public void loadWaites() {
        try {
            waiteFileScanner = new Scanner(waiteFile);

            while (waiteFileScanner.hasNextLine()) {

                String line = waiteFileScanner.nextLine();
                String[] Data;
                Data = line.split("\\|");

                String waiteClass = Data[0];
                Boolean waiteAisle = boolParse(Data[1]);
                Boolean waiteTable = boolParse(Data[2]);
                Boolean waiteForward = boolParse(Data[3]);
                Boolean waiteEase = boolParse(Data[4]);
                String seatBooking = Data[5];


                Waite Waite = new Waite(Integer.valueOf(waiteClass),  waiteAisle, waiteTable, waiteForward, waiteEase, seatBooking);
                Waites.add(Waite);
            }


        } catch (FileNotFoundException e) {
            //There a chance of this, Not really an issue
        } finally {

            //Make sure the Scanner is closed
            if (waiteFileScanner != null ) {
                waiteFileScanner.close();
            }
            //Also mark as loaded, Even if we failed to load from the file we still attempted to do it;
            dataLoaded = true;
        }

    }

    public void printWaites() {

        if (!(Waites.size() > 0)) {
            println("Waiting list is empty!");
            return;
        }


            String formt;
            println("========================================================================================================");
            println("| Email                                | Class | Aisle Seat | Table | seat Direction | Ease of Access? |");
            println("========================================================================================================");
            formt = "| %-32s     | %-3s   | %-3s        | %-3s   | %-9s      | %-3s             |\n";

        for (Waite Waite : Waites) {

                String resultClass;

            if (Waite.getWaiteeClass() == -1 ) {
                resultClass = "-";
            } else if (Waite.getWaiteeClass() == 1 ) {
                    resultClass = "1st";
                } else {
                    resultClass = "2nd";
                }


            String AisleSeat;
            if (Waite.getWaiteeAisle() == null ) {
                AisleSeat = "-";
            } else if (Waite.getWaiteeAisle()) {
                AisleSeat = "Yes";
            } else {
                AisleSeat = "No";
            }

            String resultTable;
            if (Waite.getWaiteeTable() == null ) {
                resultTable = "-";
            } else if (Waite.getWaiteeTable()) {
                resultTable = "Yes";
            } else {
                resultTable = "No";
            }


                String resultDirection;
            if (Waite.getWaiteeForward() == null ) {
                resultDirection = "-";
            } else  if (Waite.getWaiteeForward()) {
                resultDirection = "Forward";
            } else {
                resultDirection = "Backwards";
            }

            String resultEase;
            if (Waite.getWaiteeEase()) {
                resultEase = "Yes";
            } else {
                resultEase = "No";
            }

            System.out.printf(formt, Waite.getWaiteeEmail(), resultClass, AisleSeat, resultTable, resultDirection, resultEase);

            }
            println("========================================================================================================");
    }


    public void process() {

        Seats seats = new Seats();
        seats.loadSeats();

        Iterator<Waite> waitesIt = Waites.iterator();
        while (waitesIt.hasNext()) {
            Waite wait = waitesIt.next();
            int waiteClass = wait.getWaiteeClass();
            Boolean waiteAisle = wait.getWaiteeAisle();
            Boolean waiteTable = wait.getWaiteeTable();
            Boolean waiteForward = wait.getWaiteeForward();
            Boolean waiteEase = wait.getWaiteeEase();
            String seatBooking = wait.getWaiteeEmail();


            for (Seat seat : seats.getSeats()) {
                if (    (!(seat.getSeatBooking().equals("UNBOOKED"))) &&
                        ((waiteClass == -1) || waiteClass == seat.getSeatClass()) &&
                        (waiteAisle == null || waiteAisle == seat.getSeatAisle()) &&
                        (waiteTable == null || waiteTable == seat.getSeatTable()) &&
                        (waiteForward == null || waiteForward == seat.getSeatForward()) &&
                        (waiteEase == seat.getSeatEase()))

                {
                    seat.setSeatBooking(seatBooking);
                    println("Seat booked: " + seat.getSeatLocation() + " For email: " + wait.getWaiteeEmail());
                    waitesIt.remove();
                    break;


                }
            }
        }
        seats.saveSeats();
    }


    public void saveWaites() {

        try {
            PrintWriter saveWriter = new PrintWriter(waiteFile);

            for (Waite Waite : Waites) {
                println(Waite.toString());
                saveWriter.println(Waite.toString());
            }

            saveWriter.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addWaite(Waite Waite) {
        Waites.add(Waite);
    }

    public List<Waite> getWaites() {
        return Waites;
    }


    public void println(String string) {
        System.out.println(string);
    }

    public Boolean boolParse(String string) {
        if (string.equals("null"))
            return null;
        return Boolean.parseBoolean(string);
    }

}
